#include "Header.h"
#include "Map.cpp"
#include <fstream>

using namespace std;

bool gameOver;
int width = 0;
int height = 0;
int lvl;
string name;
int limitSave = 0;
const int len = 40, strings = 14;
const char ch = '\n';
char mass[strings][len];

//eDirection dir;

void Setup();

void save_game(int score, string name);

void show_results();

int main()
{
	Setup();
	system("pause");
	lvl = 1;//����� ������
	new_Map map(width, height, lvl);


	while(!map.condition()) {
		if (map.end_lvl()) {
			system("cls");
			if (map.get_lvl() == 3) {
				cout << "You have completed the game! Save result?"; // ��������
				save_game(map.Score(), name);
				break;
			}
			else {
				cout << "You have completed the level!\nDo you want to continue? (y/n)";//��������
			}
		}
		map.Draw();
		map.Input();
		map.Logic();
	}
	
	return 0;
}

void save_game(int score, string name) {
	
	ifstream out("save.txt", ios::in);

	if (!out.is_open())
		cout << "Save game error!" << endl;

	for (int i = 0; i < strings; i++)
	{
		out.getline(mass[i], len, ch); 
	}
	out.close(); 

	ofstream in("save.txt");
	if (!in.is_open()) {
		cout << "Save game error!" << endl;
	}
	else {
		in << "Player " << name << " scored " << score << " points." << endl;
	}

	for (int i = 0; i < 3; i++) {
		in << mass[i] << endl;
	}
	in.close();
}

void show_results() {

	ifstream lo("save.txt", std::ios::in);

	if (!lo.is_open()) 
		cout << "Impossible to show results!" << endl;

	for (int i = 0; i < strings; i++)
	{
		lo.getline(mass[i], len, ch); //��������� ������ � ������
		cout << i + 1 << ") " << mass[i] << endl;
	}
	lo.close();

}

void Setup()
{
	lvl = 0;
	cout << "Enter your nickname: ";
	cin >> name;
	while (lvl < 1 || lvl > 3) {
		cout << "\n1 - easy\n2 - normal\n3 - hard\nChoose level: ";
		cin >> lvl;
	}
	/*if (lvl == 4){
		
		while (width <= 20 || width > 50) {
			cout << "Enter map width(20 - 50): ";
			cin >> width;
		}
		while (height <= 20 || height > 50) {
			cout << "Enter map height(20 - 50): ";
			cin >> height;
		}

	}
	else {
		width = 20;
		height = 20;
	}*/
}