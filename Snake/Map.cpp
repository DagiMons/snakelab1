#include "Header.h"
#include "Move_Enum.cpp"

using namespace std;


struct new_Map {
	
	new_Map(int width, int height, int lvl): width(width), height(height), lvl(lvl) {
		gameOver = false;
		dir = STOP;
		x = width / 2 - 1;
		y = height / 2 - 1;
		fruitX = rand() % width;
		fruitY = rand() % height;
		score = 0;
		lvlEND = false;
		nTail = 0;
		
		HM_score();
	}

	int get_lvl() {
		return lvl;
	}

	bool end_lvl() {
		return lvlEND;
	}

	int max_Score() {
		return maxScore;
	}

	int Score() {
		return score;
	}

	bool condition() {
		return gameOver;
	}

	void Input()
	{
		if (_kbhit()) // if pressed a button
		{
			switch (_getch()) // wich button 
			{
			case 'a':
				dir = LEFT;
				break;
			case 'd':
				dir = RIGHT;
				break;
			case 'w':
				dir = UP;
				break;
			case 's':
				dir = DOWN;
				break;
			case 'x':
				gameOver = true;
				break;
			case 27:
				system("pause");
				break;
			}
		}
	}

	void Draw() {
		lvlEND = false;
		//����� ������� ��������� ��������
		switch (lvl){
		case 1:
			Draw_lvl1();
			break;
		case 2:
			Draw_lvl2();
			break;
		case 3:
			Draw_lvl3();
			break;
		}
		
	}

	void Logic()
	{
		int prevx = tailX[0];
		int prevY = tailY[0];
		tailX[0] = x;
		tailY[0] = y;
		int prev2x, prev2y;

		for (int i = 1; i < nTail; i++)
		{
			prev2x = tailX[i];
			prev2y = tailY[i];
			tailX[i] = prevx;
			tailY[i] = prevY;
			prevx = prev2x;
			prevY = prev2y;
		}
		switch (dir)
		{
		case LEFT:
			x--;
			break;
		case RIGHT:
			x++;
			break;
		case UP:
			y--;
			break;
		case DOWN:
			y++;
			break;
		}
		//if (x > width-2 || x < 0 || y > height-1 || y < 0)
			//gameOver = true; ��� �������� � �����

		if (x >= width - 1)
			x = 0;
		else if (x < 0)
			x = width - 2;

		if (y >= height)
			y = 0;
		else if (y < 0)
			y = height - 1;

		for (int i = 0; i < nTail; i++) {
			if (tailX[i] == x && tailY[i] == y)
				gameOver = true;
		}

		if (x == fruitX && y == fruitY)
		{
			score += 10;
			if (score == maxScore) {
				next_lvl();
				return;
			}
			fruitX = rand() % width;
			fruitY = rand() % height;
			nTail++;
			
		}
	}

	private :
		int width;
		int height;
		int x, y, fruitX, fruitY, score;
		int tailX[100], tailY[100];
		int nTail;
		bool gameOver, lvlEND;
		int lvl;
		int maxScore;
		eDirection dir;

		void next_lvl() {
			if (lvl < 4) {
				lvl++;
				HM_score();
				lvlEND = true;
			}
		}

		void Draw_lvl1()
		{
			system("cls"); // clear console

			for (int i = 0; i < width + 1; i++)
				cout << "#";
			cout << endl;

			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					if (j == 0 || j == width - 1)
						cout << "#";
					if (i == y && j == x)
						cout << "0";
					else if (i == fruitY && j == fruitX)
						cout << "F";
					else
					{
						bool print = false;
						for (int k = 0; k < nTail; k++)
						{
							if (tailX[k] == j && tailY[k] == i)
							{
								cout << "o";
								print = true;
							}
						}
						if (!print)
							cout << " ";
					}
				}
				cout << endl;
			}

			for (int i = 0; i < width + 1; i++)
				cout << "#";
			cout << endl << "Score = " << score;
			
		}

		void Draw_lvl2(){}

		void Draw_lvl3() {}

		void HM_score() { //How many score?
			switch (lvl) {
			case 1:
				maxScore = 100;
				break;
			case 2:
				maxScore = 200;
				break;
			case 3:
				maxScore = 280;
				break;
			}
		}
};